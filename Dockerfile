FROM elixir:1.12-alpine

RUN apk add git
RUN mix local.hex --force

WORKDIR /code