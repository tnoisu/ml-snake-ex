defmodule SnakeTest do
  use ExUnit.Case
  doctest Snake

  test "it moves" do
    position = Vector2.create(4, 4)
    velocity = Vector2.create(1, 0)
    snake = Snake.create(position, velocity) |> Snake.move()

    assert Snake.position(snake) == Vector2.create(5, 4)
  end
end
