defmodule Vector2Test do
  use ExUnit.Case
  doctest Vector2

  test "adds two vectors" do
    v1 = Vector2.create(13, 37)
    v2 = Vector2.create(-13, 1)
    result = Vector2.add(v1, v2)

    assert result == {0, 38}
  end

  test "muliplication by number" do
    v = Vector2.create(0, 1)

    assert Vector2.mul(v, -1) == Vector2.create(0, -1)
  end

  test "normalize" do
    v =
      Vector2.create(0, -2)
      |> Vector2.normalize()

    assert v == Vector2.create(0, -1)
  end

  test "turns left" do
    v = Vector2.create(1, 0)
    assert Vector2.turn_left(v) == Vector2.create(0, -1)
  end

  test "turns right" do
    v = Vector2.create(1, 0)
    assert Vector2.turn_right(v) == Vector2.create(0, 1)
  end

  test "turns left 45" do
    v = Vector2.create(1, 0)
    assert Vector2.turn_left45(v) == Vector2.create(1, -1)

    v = Vector2.create(1, -1)
    assert Vector2.turn_left45(v) == Vector2.create(0, -1)
  end

  test "distance" do
    v1 = Vector2.create(0, 0)
    v2 = Vector2.create(10, 0)

    assert Vector2.distance(v1, v2) == 10.0
  end
end
