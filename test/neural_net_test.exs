defmodule NeuralNetTest do
  use ExUnit.Case
  doctest NeuralNet

  test "to vector" do
    list = Enum.map(1..12, &Function.identity/1)
    nn = NeuralNet.create([2, 2, 2], list)

    assert NeuralNet.to_vector(nn) == list
  end
end
