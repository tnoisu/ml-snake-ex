defmodule GameBoardTest do
  use ExUnit.Case
  doctest GameBoard

  def test_objects do
    field_size = Vector2.create(10, 10)
    snake_pos = Vector2.create(3, 4)
    food_pos = Vector2.create(6, 6)
    snake = Snake.create(snake_pos, Vector2.create(1, 0))
    board = GameBoard.create(field_size, snake, food_pos)

    {snake_pos, food_pos, board}
  end

  test "returns cell contents" do
    {snake_pos, food_pos, board} = test_objects()

    assert GameBoard.cell(board, Vector2.create(0, 0)) == :empty
    assert GameBoard.cell(board, food_pos) == :food
    assert GameBoard.cell(board, snake_pos) == :snake
  end

  test "checks out of bounds" do
    {_, _, board} = test_objects()

    assert GameBoard.cell(board, Vector2.create(-1, 0)) == :out_of_bounds
    assert GameBoard.cell(board, Vector2.create(0, -1)) == :out_of_bounds
  end

  test "returns next nonempty cell" do
    {_, _, board} = test_objects()
    test_pos = Vector2.create(6, 4)

    result = GameBoard.next_nonempty_cell(board, test_pos, Vector2.create(0, 1))
    assert result == {:food, 2}

    result = GameBoard.next_nonempty_cell(board, test_pos, Vector2.create(0, -1))
    assert result == {:out_of_bounds, 5}

    result = GameBoard.next_nonempty_cell(board, test_pos, Vector2.create(1, 0))
    assert result == {:out_of_bounds, 4}

    result = GameBoard.next_nonempty_cell(board, test_pos, Vector2.create(-1, 0))
    assert result == {:snake, 3}
  end
end
