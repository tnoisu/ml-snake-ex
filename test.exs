field_size = Vector2.create(20, 20)

dimensions = [14, 5, 3]
population_size = 30
elitism = 0.1
mutation_rate = 0.25
max_steps = 200

trainer = AITrainer.create(
  field_size, population_size, dimensions, elitism, mutation_rate, max_steps)

AITrainer.run(trainer)
