defmodule GameBoard do
  @moduledoc false

  def create(field_size, snake, food) do
    board = Map.put(%{}, Food.position(food), :food)
    board = put_snake(board, Snake.body(snake))

    {field_size, board}
  end

  def cell(board, pos) do
    {field_size, board} = board

    if out_of_bounds(field_size, pos) do
      :out_of_bounds
    else
      Map.get(board, pos, :empty)
    end
  end

  def next_nonempty_cell(board, position, velocity) do
    {type, pos} = next_nonempty_cell_helper(board, Vector2.add(position, velocity), velocity)
    {type, Vector2.distance(position, pos)}
  end

  defp next_nonempty_cell_helper(board, position, velocity) do
    case cell(board, position) do
      :empty ->
        position = Vector2.add(position, velocity)
        next_nonempty_cell_helper(board, position, velocity)

      x ->
        {x, position}
    end
  end

  defp out_of_bounds({max_x, max_y}, {x, y}) do
    x < 0 || x >= max_x || y < 0 || y >= max_y
  end

  defp put_snake(board, body) do
    case body do
      [] ->
        board

      [part | body] ->
        board = Map.put(board, part, :snake)
        put_snake(board, body)
    end
  end
end
