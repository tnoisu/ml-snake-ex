defmodule AIPlayer do
  @moduledoc """
  AI player representation
  """

  def create(neural_net) do
    turn_treshold = 0.8

    {neural_net, turn_treshold}
  end

  defp cell_type_to_number(type) do
    case type do
      :out_of_bounds -> 0
      :empty -> 1
      :snake -> 2
      :food -> 3
    end
  end

  defp evaluate_output(output, velocity, turn_treshold) do
    [do_nothing | output] = output
    [turn_left | output] = output
    [turn_right | _] = output

    should_turn_left =
      turn_left > do_nothing &&
        turn_left > turn_right &&
        turn_left > turn_treshold

    should_turn_right =
      turn_right > do_nothing &&
        turn_right > turn_left &&
        turn_right > turn_treshold

    cond do
      should_turn_left -> Vector2.turn_left(velocity)
      should_turn_right -> Vector2.turn_right(velocity)
      true -> velocity
    end
  end

  def get_velocity(player, board, snake) do
    {neural_net, turn_treshold} = player
    position = Snake.position(snake)
    velocity = Snake.velocity(snake)
    reverse = Vector2.mul(velocity, -1)

    {vectors, _} =
      Enum.reduce(1..8, {[], velocity}, fn _, {result, velocity} ->
        result =
          if velocity != reverse do
            [velocity | result]
          else
            result
          end

        velocity = Vector2.turn_left45(velocity)
        {result, velocity}
      end)

    input =
      Enum.map(vectors, fn vector ->
        {type, dist} = GameBoard.next_nonempty_cell(board, position, vector)
        type = cell_type_to_number(type)

        [type, dist]
      end)
      |> List.flatten()

    output = NeuralNet.run(neural_net, input)
    evaluate_output(output, velocity, turn_treshold)
  end
end
