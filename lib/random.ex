defmodule Random do
  @moduledoc """
  PRNG
  """

  def random_int(min, max) do
    random_float(min, max) |> round
  end

  def random_float(min, max) do
    diff = max - min
    :rand.uniform() * diff + min
  end
end
