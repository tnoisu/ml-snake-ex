defmodule AISnake do
  @moduledoc """
  Solution for an AI snake problem
  """

  defstruct [:neural_net, :field_size, :dimensions, :max_steps]

  def create(dimensions, field_size, max_steps) do
    nn = NeuralNet.random(dimensions)

    %AISnake{
      neural_net: nn,
      field_size: field_size,
      dimensions: dimensions,
      max_steps: max_steps
    }
  end

  def fitness(ai_snake) do
    player = AIPlayer.create(ai_snake.neural_net)
    game = Game.create(ai_snake.field_size, player, ai_snake.max_steps)

    {_, score} = Game.run(game)
    score
  end

  def crossover(one, two, mutation_rate) do
    v1 = NeuralNet.to_vector(one.neural_net)
    v2 = NeuralNet.to_vector(two.neural_net)

    result =
      crossover_vectors(v1, v2)
      |> add_noise(mutation_rate)

    neural_net = NeuralNet.create(one.dimensions, result)

    %AISnake{one | neural_net: neural_net}
  end

  def add_noise(vector, prob) do
    Enum.map(vector, fn x ->
      if Random.random_float(0, 1) < prob do
        x + Random.random_float(-0.3, 0.3)
      else
        x
      end
    end)
  end

  defp crossover_vectors(v1, v2) when length(v1) == length(v2) do
    crossover_vectors_helper(v1, v2, [])
  end

  defp crossover_vectors_helper(v1, v2, result) do
    case v1 do
      [] ->
        Enum.reverse(result)

      [x | v1] ->
        [y | v2] = v2
        dice = Random.random_float(0, 1)

        if dice <= 0.5 do
          crossover_vectors_helper(v1, v2, [x | result])
        else
          crossover_vectors_helper(v1, v2, [y | result])
        end
    end
  end
end
