defmodule AITrainer do
  @moduledoc """
  AI snake trainer
  """

  defstruct [:ga, :generation, :best_fitness]

  def create(
        field_size,
        population_size,
        dimensions,
        elitism,
        mutation_rate,
        max_steps
      ) do
    population =
      Enum.map(1..population_size, fn _ ->
        AISnake.create(dimensions, field_size, max_steps)
      end)

    ga = GeneticAlgorithm.create(AISnake, population, elitism, mutation_rate)

    %AITrainer{
      ga: ga,
      generation: 1,
      best_fitness: nil
    }
  end

  def step(trainer) do
    %AITrainer{ga: ga, generation: generation} = trainer

    {ga, fitnesses} = GeneticAlgorithm.step(ga)
    {_, best_fitness} = Enum.at(fitnesses, 0)
    IO.puts("Generation: #{generation}, fitness: #{best_fitness}")

    %AITrainer{ga: ga, generation: generation + 1, best_fitness: best_fitness}
  end

  def run(trainer) do
    trainer = step(trainer)
    run(trainer)
  end

  def run_steps(trainer, steps) do
    if steps <= 0 do
      trainer
    else
      trainer = step(trainer)
      run_steps(trainer, steps - 1)
    end
  end
end
