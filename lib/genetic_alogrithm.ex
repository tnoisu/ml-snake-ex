defmodule GeneticAlgorithm do
  @moduledoc """
  Asyncronous genetic algorithm module
  """

  defstruct [:population, :elitism, :mutation_rate, :solution_module]

  def create(solution_module, population, elitism, mutation_rate) do
    %GeneticAlgorithm{
      population: population,
      elitism: elitism,
      mutation_rate: mutation_rate,
      solution_module: solution_module
    }
  end

  def step(ga) do
    %GeneticAlgorithm{
      population: population,
      elitism: elitism,
      mutation_rate: mutation_rate,
      solution_module: solution_module
    } = ga

    fitnesses = compute_fitnesses(solution_module, population)
    sorted = sort_by_fitness(fitnesses)
    to_breed = Enum.map(sorted, &elem(&1, 0))
    population = breed(solution_module, to_breed, mutation_rate, elitism)

    {%GeneticAlgorithm{ga | population: population}, sorted}
  end

  def compute_fitnesses(solution_module, population, timeout \\ 20_000) do
    tasks =
      Enum.map(population, fn x ->
        Task.async(fn -> {x, solution_module.fitness(x)} end)
      end)

    Task.await_many(tasks, timeout)
  end

  def sort_by_fitness(fitnesses) do
    Enum.sort(fitnesses, fn {_, fitness_x}, {_, fitness_y} ->
      fitness_x > fitness_y
    end)
  end

  def breed(solution_module, population, mutation_rate, elitism) do
    population_size = length(population)
    num_of_breeders = round(population_size * elitism)
    breeders = Enum.take(population, num_of_breeders)

    tasks =
      Enum.map(num_of_breeders..(population_size - 1), fn _ ->
        one = Random.random_int(0, num_of_breeders - 1)
        two = Random.random_int(0, num_of_breeders - 1)
        x = Enum.at(breeders, one)
        y = Enum.at(breeders, two)

        Task.async(fn ->
          solution_module.crossover(x, y, mutation_rate)
        end)
      end)

    next_gen = Task.await_many(tasks, 20_000)

    breeders ++ next_gen
  end
end
