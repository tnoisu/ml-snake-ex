defmodule Game do
  @moduledoc """
  Snake game
  """

  def create(field_size, player, max_steps) do
    score = 0
    steps = 0
    initial_position = Vector2.create(0, 0)
    velocity = Vector2.create(1, 0)
    snake = Snake.create(initial_position, velocity)

    food = Food.create(field_size)

    {field_size, snake, food, player, score, max_steps, steps}
  end

  def run(game) do
    case update(game) do
      {:game_over, score} -> {:game_over, score}
      game -> run(game)
    end
  end

  def update(game) do
    {field_size, snake, food, player, score, max_steps, steps} = game

    if is_game_over(field_size, snake) || steps >= max_steps do
      steps_left = round((max_steps - steps) / 10)
      total_score = score * 100 - steps_left
      {:game_over, total_score}
    else
      {new_snake, new_food, new_score, steps} =
        update_objects(field_size, snake, food, score, steps)

      board = GameBoard.create(field_size, new_snake, new_food)
      new_velocity = AIPlayer.get_velocity(player, board, new_snake)

      new_snake = Snake.set_velocity(new_snake, new_velocity)

      {field_size, new_snake, new_food, player, new_score, max_steps, steps}
    end
  end

  defp out_of_bounds({max_x, max_y}, {x, y}) do
    x < 0 || x >= max_x ||
      y < 0 || y >= max_y
  end

  defp is_game_over(field_size, snake) do
    Snake.is_dead(snake) || out_of_bounds(field_size, Snake.position(snake))
  end

  defp update_objects(field_size, snake, food, score, steps) do
    if Snake.position(snake) == Food.position(food) do
      snake = Snake.grow(snake)
      food = Food.create(field_size)
      score = score + 1
      steps = 0
      {snake, food, score, steps}
    else
      snake = Snake.move(snake)
      steps = steps + 1
      {snake, food, score, steps}
    end
  end
end
