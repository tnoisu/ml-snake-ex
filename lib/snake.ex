defmodule Snake do
  @moduledoc false

  def create(position, velocity) do
    {[position], velocity}
  end

  def position({body, _}) do
    [position | _] = body
    position
  end

  def velocity({_, velocity}) do
    velocity
  end

  def body({body, _}) do
    body
  end

  def is_dead({body, _}) do
    [head | tail] = body

    Enum.find(tail, fn x -> head == x end) != nil
  end

  def set_velocity({body, _}, velocity) do
    {body, velocity}
  end

  def move({body, velocity}) do
    [head | _] = body
    head = Vector2.add(head, velocity)
    body = List.delete_at(body, -1)

    {[head | body], velocity}
  end

  def grow({body, velocity}) do
    [head | _] = body
    head = Vector2.add(head, velocity)

    {[head | body], velocity}
  end
end
