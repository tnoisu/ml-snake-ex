defmodule Vector2 do
  @moduledoc """
  Representation of 2D vectors and points
  """

  def create(x, y) do
    {x, y}
  end

  def add({x1, y1}, {x2, y2}) do
    {x1 + x2, y1 + y2}
  end

  def mul({x, y}, n) do
    {x * n, y * n}
  end

  def turn_left({x, y}) do
    {y, x * -1}
  end

  def turn_right({x, y}) do
    {y * -1, x}
  end

  def turn_left45(v) do
    left = turn_left(v)
    add(v, left) |> normalize
  end

  def map({x, y}, fun) do
    {fun.(x), fun.(y)}
  end

  defp abs_value(v) do
    map(v, &Kernel.abs/1)
  end

  defp divide({x1, y1}, {x2, y2}) do
    x =
      if x2 == 0 do
        0
      else
        x1 / x2
      end

    y =
      if y2 == 0 do
        0
      else
        y1 / y2
      end

    {x, y}
  end

  def normalize(v) do
    divide(v, abs_value(v))
  end

  def distance({x1, y1}, {x2, y2}) do
    :math.sqrt(:math.pow(x1 - x2, 2) + :math.pow(y1 - y2, 2))
  end
end
