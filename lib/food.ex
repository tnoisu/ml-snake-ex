defmodule Food do
  @moduledoc false

  def create(field_size) do
    random_position(field_size)
  end

  def position(position) do
    position
  end

  defp random_position({max_x, max_y}) do
    x = round(:rand.uniform() * (max_x - 1))
    y = round(:rand.uniform() * (max_y - 1))

    Vector2.create(x, y)
  end
end
