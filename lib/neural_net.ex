defmodule NeuralNet do
  @moduledoc """
  Feedforward neural network
  """

  def create(dimensions, vector) do
    [hd | tl] = dimensions

    {layers, layer_dims, _, _} =
      Enum.reduce(tl, {[], [], hd, vector}, fn current_layer, acc ->
        {layers, layer_dims, prev_layer, vector} = acc
        weights_len = prev_layer * current_layer
        biases_len = current_layer

        {weights_list, vector} = Enum.split(vector, weights_len)
        {biases_list, vector} = Enum.split(vector, biases_len)

        weights =
          Matrix.reshape(weights_list, {current_layer, prev_layer})
          |> Matrix.transpose()

        biases = Matrix.reshape(biases_list, {1, biases_len})

        layers = [{weights, biases} | layers]
        layer_dims = [{prev_layer, current_layer} | layer_dims]

        {layers, layer_dims, current_layer, vector}
      end)

    {Enum.reverse(layers), Enum.reverse(layer_dims)}
  end

  def random(dimensions) do
    [hd | tl] = dimensions

    {layers, layer_dims, _} =
      Enum.reduce(tl, {[], [], hd}, fn current_layer, acc ->
        {layers, layer_dims, prev_layer} = acc
        weights_len = prev_layer * current_layer
        biases_len = current_layer

        weights_list = random_vector(weights_len)
        biases_list = random_vector(biases_len)

        weights =
          Matrix.reshape(weights_list, {current_layer, prev_layer})
          |> Matrix.transpose()

        biases = Matrix.reshape(biases_list, {1, biases_len})

        layers = [{weights, biases} | layers]
        layer_dims = [{prev_layer, current_layer} | layer_dims]

        {layers, layer_dims, current_layer}
      end)

    {Enum.reverse(layers), Enum.reverse(layer_dims)}
  end

  def to_vector({layers, layer_dims}) do
    to_vector_helper(layers, layer_dims, [])
  end

  defp to_vector_helper(layers, layer_dims, acc) do
    case layers do
      [] ->
        Enum.reverse(acc) |> List.flatten()

      [layer | layers] ->
        [dims | layer_dims] = layer_dims
        {weights, biases} = layer
        {rows, cols} = dims

        weights_list =
          Matrix.transpose(weights)
          |> Matrix.reshape(rows * cols)

        biases_list = Matrix.reshape(biases, length(biases))
        acc = [weights_list | acc]
        acc = [biases_list | acc]

        to_vector_helper(layers, layer_dims, acc)
    end
  end

  defp random_vector(length) do
    Enum.map(1..length, fn _ ->
      Random.random_float(-1, 1)
    end)
  end

  def run(nn, input) do
    {layers, _} = nn
    len = length(input)
    relu = fn x -> max(x, 0) end

    output =
      Enum.reduce(layers, Matrix.reshape(input, {1, len}), fn layer, prev_activations ->
        {weights, biases} = layer

        Matrix.mult(weights, prev_activations)
        |> Matrix.add(biases)
        |> Matrix.map(relu)
      end)

    Matrix.reshape(output, 3)
  end
end
